# Higher speed IO test chip

This is an alpha of a set of IO pad cells that should allow higher speed IO than the
original Skywater IO cells. As no real guidelines are available for designing own ESD and
latch-up the design of the Skywater has been used as inspiration for the design of the
combine output driver/ESD clamp. Currently only a snapback ESD clamp is used and no active
clamp is used on the vdd/iovdd nets.

## Cell set

A (minimal set) of IO pad cells is included that are meant to be put abutted next to each.This will generate an IO ring with lanes for the iovdd, iovss, vdd and vss DC connection needed by all the IO cells.
Included cells

* IOPadVdd: the pad for the 1.8V core vdd connection;
* IOPadVss: the pad for the core vss connection;
* IOPadIOVdd: the pad for the 3.3V IO vdd connection
* IOPadIOVss: the pad for the IO vss connection
* IOPadIn: the pad for an input pin to the core; the core 1.8V pins:
    * s (sense): the data sensed on the IO pad
* IOPadOut: the pad for an output of the core; the core 1.8V pins:
    * d (drive): the logic value for the output
    * de (drive enable): wether to enable the output. If value is logic 0 the pad will be in high impedance state
* IOPadInOut: the for an input/output to/from the core; the core 1.8V pins:
    * s (sense): the data sensed on the IO pad
    * d (drive): the logic value for the output
    * de (drive enable): wether to enable the output. If value is logic 0 the pad will be in high impedance state

## Design top (user_analog_project_wrapper)

This project uses the caravan top cell using the 11 analog connections to connect to the IO pads without external ESD protection load etc. on the signal.

![user_analog_project_wrapper](user_analog_project_wrapper.png)

The design contains two blocks located near the top of the design. The first at the top is a set of IO pads. This set has all signals including the core 1.8V signal connected out to the caravan analog pads. The second below is a set making a 5 stage ring-oscillator with IO pads; each stage consist of a IOPadIn and IOPadOut IO cell.

Following is a table with the signal and corresponding caravan pin on the design:

| caravan pin   | internal signal| cell         | Description                               |
|---------------|----------------|--------------|-------------------------------------------|
| io_analog[0]  | vss            | IOBlock&IORO | core ground                               |
| io_analog[1]  | vdd            | IOBlock&IORO | core 1.8V supply                          |
| io_analog[2]  | ioout_pad      | IOBlock      | pad of IOPadOut                           |
| io_analog[3]  | ioinout_pad    | IOBlock      | pad of IOPadInOut                         |
| io_analog[4]  | d_core         | IOBlock      | shared d signal for IOPadOut & IOPadInOut |
| io_analog[5]  | de_core        | IOBlock      | shared de signal for IOPadOut & IOPadInOut|
| io_analog[6]  | ioinout_core   | IOBlock      | s signal of IOPadInOut                    |
| io_analog[7]  | ioin_core      | IOBlock      | s signal of IOPadIn                       |
| io_analog[8]  | ioin_pad       | IOBlock      | pad of IOPadIn                            |
| io_analog[9]  | iovdd          | IOBlock&IORO | IO 3.3V supply                            |
| io_analog[10] | iovss          | IOBlock&IORO | IO ground                                 |
| gpio_analog[4]| ro_de          | IORO         | shared de signal of all IOPadOut cells    |
| gpio_analog[5]| ro_en          | IORO         | RO enable signal                          |
| gpio_noesd[7] | ro_out         | IORO         | RO output                                 |

## IOBlock

Close up of the subcell with the connected out cells:

![IOBlock](IOBlock.png)

## IORO

Close up of the subcell with the ring-oscillator with stages from IO cells:

![IORO](IORO.png)

Next to the supply/ground signals the block has three external signals:

* ro_de: this is connect to the de signal of all 5 IOPadOut IO pad cells so they can all be put in high impedance state
* ro_en: this signal allows to enable/disable the oscilation of the RO. This signal is connected to a NAND2 gate, the other input is connected to the s signal of the first IOPadIn cell, the output of the NAND2 is connected the the d signal of the last IOPadOut cell.
* ro_out: this is the buffered output of the RO. It is the output of a buf_x4 cell with the input the s signal of the first IOPadIn cell.

## Source

The top level is fully generated from python code in the `doitcode` subdirectory. pydoit is used to generate the desig with the provided `dodo.py` file in the top directory. The code depends on some external modules that are assumed to be installed:

* [PDKMaster](https://gitlab.com/Chips4Makers/PDKMaster): python framework (under heavy development) to ease generation of circuits and corresponding DRC compliant layout. This based on a description of a technology with python souce code to allow easy porting to different technologies.
* [c4m-flexcell](https://gitlab.com/Chips4Makers/c4m-flexcell): a (currently minimal) standard cell library based on PDKMaster
* [c4m-flexio](https://gitlab.com/Chips4Makers/c4m-flexio): the source for the IO pad cells used for this design
* [c4m-pdk-sky130](https://gitlab.com/Chips4Makers/c4m-pdk-sky130): the source for the PDKMaster based PDK for the Sky130 technology.

## License

The resulting GDS files is released under [the LGPL 2.1 or later](https://spdx.org/licenses/LGPL-2.1-or-later.html) license. Some of the source code to generate the GDS is under [the GPL 2.0 or later](https://spdx.org/licenses/GPL-2.0-or-later.html) license.
