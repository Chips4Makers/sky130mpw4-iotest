# SPDX-License-Identifier: LGPL-2.1-or-later
from pdkmaster.technology import geometry as _geo


__all__ = ["boundary", "toppins"]


# The metal3 pins for the IOs
boundary = _geo.Rect(left=0.0, bottom=0.0, right=2920.0, top=3520.0)
toppins = {
    "io_analog[0]": _geo.Rect(left=2911.50, bottom=3389.92, right=2924.00, top=3414.92),
    "io_analog[1]": _geo.Rect(left=2832.97, bottom=3511.50, right=2857.97, top=3524.00),
    "io_analog[2]": _geo.Rect(left=2326.97, bottom=3511.50, right=2351.97, top=3524.00),
    "io_analog[3]": _geo.Rect(left=2066.97, bottom=3511.50, right=2091.97, top=3524.00),
    "io_analog[4]": _geo.Rect(left=1594.97, bottom=3511.50, right=1619.97, top=3524.00),
    "io_analog[5]": _geo.Rect(left=1086.47, bottom=3511.50, right=1111.47, top=3524.00),
    "io_analog[6]": _geo.Rect(left=827.97, bottom=3511.50, right=852.97, top=3524.00),
    "io_analog[7]": _geo.Rect(left=600.97, bottom=3511.50, right=625.97, top=3524.00),
    "io_analog[8]": _geo.Rect(left=340.97, bottom=3511.50, right=365.97, top=3524.00),
    "io_analog[9]": _geo.Rect(left=80.97, bottom=3511.50, right=105.97, top=3524.00),
    "io_analog[10]": _geo.Rect(left=-4.00, bottom=3401.21, right=8.50, top=3426.21),
    "io_oeb[13]": _geo.Rect(left=2917.60, bottom=2947.36, right=2924.00, top=2947.92),
    "io_out[13]": _geo.Rect(left=2917.60, bottom=2941.45, right=2924.00, top=2942.01),
    "io_in[13]": _geo.Rect(left=2917.60, bottom=2935.54, right=2924.00, top=2936.10),
    "io_in_3v3[13]": _geo.Rect(left=2917.60, bottom=2929.63, right=2924.00, top=2930.19),
    "gpio_noesd[6]": _geo.Rect(left=2917.60, bottom=2923.72, right=2924.00, top=2924.28),
    "gpio_analog[6]": _geo.Rect(left=2917.60, bottom=2917.81, right=2924.00, top=2918.37),
    "io_oeb[12]": _geo.Rect(left=2917.60, bottom=2500.25, right=2924.00, top=2500.81),
    "io_out[12]": _geo.Rect(left=2917.60, bottom=2494.34, right=2924.00, top=2494.90),
    "io_in[12]": _geo.Rect(left=2917.60, bottom=2488.43, right=2924.00, top=2488.99),
    "io_in_3v3[12]": _geo.Rect(left=2917.60, bottom=2482.52, right=2924.00, top=2483.08),
    "gpio_noesd[5]": _geo.Rect(left=2917.60, bottom=2476.61, right=2924.00, top=2477.17),
    "gpio_analog[5]": _geo.Rect(left=2917.60, bottom=2470.70, right=2924.00, top=2471.26),
    "io_oeb[11]": _geo.Rect(left=2917.60, bottom=2278.14, right=2924.00, top=2278.70),
    "io_out[11]": _geo.Rect(left=2917.60, bottom=2272.23, right=2924.00, top=2272.79),
    "io_in[11]": _geo.Rect(left=2917.60, bottom=2266.32, right=2924.00, top=2266.88),
    "io_in_3v3[11]": _geo.Rect(left=2917.60, bottom=2260.41, right=2924.00, top=2260.97),
    "gpio_noesd[4]": _geo.Rect(left=2917.60, bottom=2254.50, right=2924.00, top=2255.06),
    "gpio_analog[4]": _geo.Rect(left=2917.60, bottom=2248.59, right=2924.00, top=2249.15),
    "io_oeb[14]": _geo.Rect(left=-4.00, bottom=2528.10, right=2.40, top=2528.66),
    "io_out[14]": _geo.Rect(left=-4.00, bottom=2534.01, right=2.40, top=2534.57),
    "io_in[14]": _geo.Rect(left=-4.00, bottom=2539.92, right=2.40, top=2540.48),
    "io_in_3v3[14]": _geo.Rect(left=-4.00, bottom=2545.83, right=2.40, top=2546.39),
    "gpio_noesd[7]": _geo.Rect(left=-4.00, bottom=2551.74, right=2.40, top=2552.30),
    "gpio_analog[7]": _geo.Rect(left=-4.00, bottom=2557.65, right=2.40, top=2558.21),
}
