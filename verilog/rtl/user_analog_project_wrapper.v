// SPDX-License-Identifier: LGPL-2.1-or-later

module user_analog_project_wrapper (
    inout vdda1,
    inout vdda2,
    inout vssa1,
    inout vssa2,
    inout vccd1,
    inout vccd2,
    inout vssd1,
    inout vssd2,

    // Wishbone Slave ports (WB MI A)
    input wb_clk_i,
    input wb_rst_i,
    input wbs_stb_i,
    input wbs_cyc_i,
    input wbs_we_i,
    input [3:0] wbs_sel_i,
    input [31:0] wbs_dat_i,
    input [31:0] wbs_adr_i,
    output wbs_ack_o,
    output [31:0] wbs_dat_o,

    // Logic Analyzer Signals
    input  [127:0] la_data_in,
    output [127:0] la_data_out,
    input  [127:0] la_oenb,

    // GPIOs
    input  [26:0] io_in,
    input  [26:0] io_in_3v3,
    output [26:0] io_out,
    output [26:0] io_oeb,

    // GPIO-analog
    inout [17:0] gpio_analog,
    inout [17:0] gpio_noesd,

    // Dedicate analog
    inout [10:0] io_analog,

    // Additional power supply ESD clamps
    inout [2:0] io_clamp_high,
    inout [2:0] io_clamp_low,

    // Independent clock (on independent integer divider)
    input   user_clock2,

    // User maskable interrupt signals
    output [2:0] user_irq
);

IOConnected ioconn (
    .iovdd(ioanalog[9]),
    .iovss(ioanalog[10]),
    .vdd(io_analog[1]),
    .vss(io_analog[0]),
    .ioin_pad(io_analog[8]),
    .ioin_core(io_analog[7]),
    .ioout_pad(io_analog[2]),
    .ioinout_pad(io_analog[3]),
    .ioinout_core(io_analog[6]),
    .d_core(io_analog[4]),
    .de_core(io_analog[5]);
    .ro_de(gpio_analog[4]),
    .ro_en(gpio_analog[5]),
    .ro_out(gpio_noesd[7]),
);

user_analog_project_wrapper_empty empty (
    .vdda1(vdda1),
    .vdda2(vdda2),
    .vssa1(vssa1),
    .vssa2(vssa2),
    .vccd1(vccd1),
    .vccd2(vccd2),
    .vssd1(vssd1),
    .vssd2(vssd2),
    .wb_clk_i(wb_clk_i),
    .wb_rst_i(wb_rst_i),
    .wbs_stb_i(wbs_stb_i),
    .wbs_cyc_i(wbs_cyc_i),
    .wbs_we_i(wbs_we_i),
    .wbs_sel_i(wbs_sel_i),
    .wbs_dat_i(wbs_dat_i),
    .wbs_adr_i(wbs_adr_i),
    .wbs_ack_o(wbs_ack_o),
    .wbs_dat_o(wbs_dat_o),
    .la_data_in(la_data_in),
    .la_data_out(la_data_out),
    .la_oenb(la_oenb),
    .io_in(io_in),
    .io_in_3v3(io_in_3v3),
    .io_out(io_out),
    .io_oeb(io_oeb),
    .gpio_analog(gpio_analog),
    .gpio_noesd(gpio_noesd),
    .io_analog(io_analog),
    .io_clamp_high(io_clamp_high),
    .io_clamp_low(io_clam_low),
    .user_clock2(user_clock2),
    .user_irq(user_irq)
)

endmodule	// user_analog_project_wrapper
