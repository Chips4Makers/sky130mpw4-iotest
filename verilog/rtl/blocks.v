// SPDX-License-Identifier: LGPL-2.1-or-later
`timesacle 1 ns / 1 ps

module IOConnected (
    inout iovdd,
    inout iovss,
    inout vdd,
    inout vss,
    inout ioin_pad,
    inout ioout_pad,
    inout ioinout_pad,
    input d_core,
    input de_core;
    output ioin_core,
    output ioinout_core,
    input ro_de,
    input ro_en,
    output ro_out,
);

endmodule // IOConnected

module user_analog_project_wrapper_empty (
    inout vdda1,
    inout vdda2,
    inout vssa1,
    inout vssa2,
    inout vccd1,
    inout vccd2,
    inout vssd1,
    inout vssd2,

    // Wishbone Slave ports (WB MI A)
    input wb_clk_i,
    input wb_rst_i,
    input wbs_stb_i,
    input wbs_cyc_i,
    input wbs_we_i,
    input [3:0] wbs_sel_i,
    input [31:0] wbs_dat_i,
    input [31:0] wbs_adr_i,
    output wbs_ack_o,
    output [31:0] wbs_dat_o,

    // Logic Analyzer Signals
    input  [127:0] la_data_in,
    output [127:0] la_data_out,
    input  [127:0] la_oenb,

    // GPIOs
    input  [26:0] io_in,
    input  [26:0] io_in_3v3,
    output [26:0] io_out,
    output [26:0] io_oeb,

    // GPIO-analog
    inout [17:0] gpio_analog,
    inout [17:0] gpio_noesd,

    // Dedicate analog
    inout [10:0] io_analog,

    // Additional power supply ESD clamps
    inout [2:0] io_clamp_high,
    inout [2:0] io_clamp_low,

    // Independent clock (on independent integer divider)
    input   user_clock2,

    // User maskable interrupt signals
    output [2:0] user_irq
);

endmodule // user_analog_project_wrapper_empty
